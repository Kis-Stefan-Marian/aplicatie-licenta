CREATE TABLE `Utilizatori` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`Rol_Website` varchar(255) NOT NULL,
	`Nume` varchar(255) NOT NULL,
	`Prenume` varchar(255) NOT NULL,
	`Nume_Utilizator` varchar(255) NOT NULL,
	`Email` varchar(255) NOT NULL,
	`Telefon` varchar(255) NOT NULL,
	`Parola` varchar(255) NOT NULL,
	`Puncte` INT(255),
	`Aprecieri` INT(255),
	`Creat_La` DATETIME(255) NOT NULL,
	`Actualizat_La` DATETIME(255) NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Detalii_Utilizatori` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`ID_Utilizator` INT NOT NULL,
	`Poza` varchar(255),
	`Descriere` varchar(255),
	`Creat_La` DATETIME(255),
	`Actualizat_La` DATETIME(255),
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Vehicule` (
	`Serie_Sasiu` INT NOT NULL AUTO_INCREMENT,
	`ID_Utilizator` INT NOT NULL,
	`Marca` varchar(255) NOT NULL,
	`Model` varchar(255) NOT NULL,
	`An_Fabricatie` varchar(255) NOT NULL,
	`Culoare` varchar(255) NOT NULL,
	`Motorizare` varchar(255) NOT NULL,
	`Puncte` INT(255),
	`Creat_La` DATETIME(255),
	`Actualizat_La` DATETIME(255),
	PRIMARY KEY (`Serie_Sasiu`)
);

CREATE TABLE `Categorii_Servicii` (
	`Cod_Categorie` INT NOT NULL AUTO_INCREMENT,
	`Denumire` varchar(255) NOT NULL,
	`Descriere` varchar(255) NOT NULL,
	`Poza` varchar(255) NOT NULL,
	`Creat_La` DATETIME(255) NOT NULL,
	`Actualizat_La` DATETIME(255) NOT NULL,
	PRIMARY KEY (`Cod_Categorie`)
);

CREATE TABLE `Servicii` (
	`Cod_Serviciu` INT NOT NULL AUTO_INCREMENT,
	`Cod_Categorie` INT NOT NULL,
	`Denumire` varchar(255) NOT NULL,
	`Descriere` varchar(255) NOT NULL,
	`Pret` FLOAT(255) NOT NULL,
	`Durata` INT(255) NOT NULL,
	`Puncte` INT(255) NOT NULL,
	`Creat_La` DATETIME(255) NOT NULL,
	`Actualizat_La` DATETIME(255) NOT NULL,
	PRIMARY KEY (`Cod_Serviciu`)
);

CREATE TABLE `Utilizator_Servicii` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`ID_Utilizator` INT NOT NULL,
	`Cod_Serviciu` INT NOT NULL,
	`Creat_La` DATETIME NOT NULL,
	`Actualizat_La` DATETIME NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Programari` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`ID_Utilizator` INT NOT NULL,
	`Ora_Inceput` DATETIME NOT NULL,
	`Ora_Sfarsit` DATETIME NOT NULL,
	`Data` DATETIME NOT NULL,
	`Serie_Sasiu` DATETIME NOT NULL,
	`Status` varchar(255) NOT NULL,
	`Motiv_Anulare` varchar(255) NOT NULL,
	`Creat_La` DATETIME(255) NOT NULL,
	`Actualizat_La` DATETIME(255) NOT NULL,
	PRIMARY KEY (`ID`)
);

ALTER TABLE `Detalii_Utilizatori` ADD CONSTRAINT `Detalii_Utilizatori_fk0` FOREIGN KEY (`ID_Utilizator`) REFERENCES `Utilizatori`(`ID`);

ALTER TABLE `Vehicule` ADD CONSTRAINT `Vehicule_fk0` FOREIGN KEY (`ID_Utilizator`) REFERENCES `Utilizatori`(`ID`);

ALTER TABLE `Servicii` ADD CONSTRAINT `Servicii_fk0` FOREIGN KEY (`Cod_Categorie`) REFERENCES `Categorii_Servicii`(`Cod_Categorie`);

ALTER TABLE `Utilizator_Servicii` ADD CONSTRAINT `Utilizator_Servicii_fk0` FOREIGN KEY (`ID_Utilizator`) REFERENCES `Utilizatori`(`ID`);

ALTER TABLE `Utilizator_Servicii` ADD CONSTRAINT `Utilizator_Servicii_fk1` FOREIGN KEY (`Cod_Serviciu`) REFERENCES `Servicii`(`Cod_Serviciu`);

ALTER TABLE `Programari` ADD CONSTRAINT `Programari_fk0` FOREIGN KEY (`ID_Utilizator`) REFERENCES `Utilizatori`(`ID`);

ALTER TABLE `Programari` ADD CONSTRAINT `Programari_fk1` FOREIGN KEY (`Serie_Sasiu`) REFERENCES `Vehicule`(`Serie_Sasiu`);








