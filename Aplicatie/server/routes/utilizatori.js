const express = require('express');
const db = require('../dbconn');
const Router = express.Router();
const bodyParser = require('body-parser');
const cors = require('cors');

Router.use(express.json());
Router.use(cors());
Router.use(express.urlencoded({
    extended: true
}));

Router.get('/', (req, res) => {
    
    res.send('Sunteti pe homepage. Pentru a insera un serviciu accesati ruta /inserare sau pentru a vizualiza serviciile accesati ruta /vizualizare');

});

Router.get('/vizualizare', (req,res) => {
    db.query("SELECT * from utilizatori", (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

module.exports = Router;