const express = require('express');
const Router = express.Router();
const fileUpload = require('express-fileupload');
const db = require('../dbconn'); 

Router.use(fileUpload());
Router.post('', (req, res) => {
    
    const file = req.files.file;
    const filename = file.name;
    const uploadPath = 'C:/Users/kisst/Desktop/Licenta/aplicatie-licenta/Aplicatie/client/public/imagini/' +filename;
    console.log("FILE"+file);
    db.query("UPDATE utilizatori SET Imagine = ? WHERE ID = 1", [filename], (err, results) => {
        if(err) {
            res.send("A aparut o eroare, reincercati.");
        } else {
            // mv() pune poza pe server - in fisierul imagini din front-end
            file.mv(uploadPath, function(err) {
                if(err) console.log(err);
                else {console.log('imagine adaugata')}
            });
            res.send("Categorie inserata cu succes.");  
        }
    })

    }
);


module.exports = Router;