const express = require('express');
const db = require('../dbconn'); 
const Router = express.Router();
const bodyParser = require('body-parser');
const cors = require('cors');

Router.use(express.json());
Router.use(cors());
Router.use(express.urlencoded({
    extended: true
}));

Router.get('/', (req, res) => {
    
    res.send('Sunteti pe homepage. Pentru a insera un serviciu accesati ruta /inserare sau pentru a vizualiza serviciile accesati ruta /vizualizare');

});

Router.post('/inserare', (req, res) => {

    let {Cod_Categorie, Denumire, Descriere, Poza, Pret, Durata, Puncte} = req.body;

    db.query("INSERT INTO servicii (Cod_Categorie, Denumire, Descriere, Poza, Pret, Durata, Puncte) VALUES (?,?,?,?,?,?,?)", [Cod_Categorie, Denumire, Descriere, Poza, Pret, Durata, Puncte], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });

});

Router.get('/vizualizarebycat/:id', (req, res) => {
    db.query("SELECT * from servicii WHERE Cod_Categorie = ?", [req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.get('/vizualizare', (req, res) => {
    db.query("SELECT * from servicii", (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.get('/vizualizare/:id', (req, res) => {
    db.query("SELECT * from servicii WHERE Cod_Serviciu = ?", [req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.put('/editare/:id', (req, res) => {

    let {Descriere, Poza, Pret, Durata, Puncte} = req.body;

    db.query("UPDATE servicii SET Descriere = ?, Pret = ?, Durata = ?, Puncte = ?, Poza = ? WHERE Cod_Serviciu = ?", [Descriere, Poza, Pret, Durata, Puncte, req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.delete('/delete/:id', (req, res) => {

    db.query("SELECT * from programari WHERE programari.Cod_Serviciu = ? AND Status = 'NEFINALIZATA'", [req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            if(results.length === 0)
            {
                db.query("DELETE utilizator_servicii FROM utilizator_servicii WHERE utilizator_servicii.Cod_Serviciu = ?", [req.params.id], (err, results) => {
                    if(err)
                    {
                        res.send(err);
                    } else {
                        db.query("DELETE servicii FROM servicii WHERE servicii.Cod_Serviciu = ?",[req.params.id], (err, results) => {
                            if(err)
                            {
                                res.send(err);
                            } else {
                                res.send("Serviciul a fost sters cu succes, deoarece nu au existat programari.");
                            }
                        });
                    }
                })
            } else {
                db.query("SELECT * from servicii s INNER JOIN programari p ON p.Cod_Serviciu = s.Cod_Serviciu WHERE s.Cod_Serviciu = ? AND p.Status != 'FINALIZATA'", [req.params.id], (err, results) => {
                    if(err)
                    {
                        res.send(err);
                    } else
                    {
                        if(results.length === 0)
                        {
                            db.query("DELETE programari from programari LEFT JOIN servicii on programari.Cod_Serviciu = servicii.Cod_Serviciu WHERE servicii.Cod_Serviciu = ?", [req.params.id], (err, results) => {
                                if(err)
                                {
                                    res.send(err);
                                } else {
                                    res.send(results);
                                }
                            });
                            db.query("DELETE servicii from servicii WHERE servicii.Cod_Serviciu = ?", [req.params.id], (err, results) => {
                                if(err)
                                {
                                    res.send(err);
                                } else {
                                    res.send(results);
                                }
                            });
                        }
                        else
                        {
                            res.send('Exista programari, nu putem sterge serviciul.');
                        }
                    }
                });
            }
        }
    });
});

module.exports = Router;