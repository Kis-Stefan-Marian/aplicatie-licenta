const express = require('express');
const db = require('../dbconn'); 
const Router = express.Router();
const bodyParser = require('body-parser');
const cors = require('cors');

Router.use(express.json());
Router.use(cors());
Router.use(express.urlencoded({
    extended: true
}));

Router.get('/', (req, res) => {
    
    res.send('Sunteti pe homepage. Pentru a insera un utilizator, accesati ruta /inserare sau pentru a vizualiza un utilizator accesati ruta /vizualizare');

});

Router.post('/inserare', (req, res) => {

    let {Denumire, Descriere, Poza} = req.body;

    db.query("INSERT INTO categorii_servicii (Denumire, Descriere, Poza) VALUES (?,?,?)", [Denumire, Descriere, Poza], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });

});

Router.get('/vizualizare', (req, res) => {
    db.query("SELECT * from categorii_servicii", (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.get('/vizualizare/:id', (req, res) => {
    db.query("SELECT * from categorii_servicii WHERE Cod_Categorie = ?", [req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.put('/editare/:id', (req, res) => {
    let descriere = req.body.Descriere;
    let poza = req.body.Poza;

    db.query("UPDATE categorii_servicii SET Descriere = ?, Poza = ? WHERE Cod_Categorie = ?", [descriere, poza, req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            res.send(results);
        }
    });
});

Router.delete('/delete/:id', (req, res) => {

    db.query("SELECT * from servicii WHERE servicii.Cod_Categorie = ?", [req.params.id], (err, results) => {
        if(err)
        {
            res.send(err);
        } else {
            if(results.length === 0)
            {
                db.query("DELETE categorii_servicii FROM categorii_servicii WHERE categorii_servicii.Cod_Categorie = ?",[req.params.id], (err, results) => {
                    if(err)
                    {
                        res.send(err);
                    } else {
                        res.send(results);
                    }
                });
            } else {
                db.query("SELECT * from servicii s INNER JOIN programari p ON p.Cod_Serviciu = s.Cod_Serviciu WHERE s.Cod_Categorie = ? AND p.Status != 'FINALIZATA'", [req.params.id], (err, results) => {
                    if(err)
                    {
                        res.send(err);
                    } else
                    {
                        if(results.length === 0)
                        {
                            db.query("DELETE programari from programari LEFT JOIN servicii on programari.Cod_Serviciu = servicii.Cod_Serviciu WHERE servicii.Cod_Categorie = ?", [req.params.id], (err, results) => {
                                if(err)
                                {
                                    res.send(err);
                                } else {
                                    res.send(results);
                                }
                            });
                            db.query("DELETE servicii from servicii LEFT JOIN categorii_servicii on categorii_servicii.Cod_Categorie = servicii.Cod_Categorie WHERE categorii_servicii.Cod_Categorie = ?", [req.params.id], (err, results) => {
                                if(err)
                                {
                                    res.send(err);
                                } else {
                                    res.send(results);
                                }
                            });
                            db.query("DELETE categorii_servicii from categorii_servicii WHERE categorii_servicii.Cod_Categorie = ?", [req.params.id], (err, results) => {
                                if(err)
                                {
                                    res.send(err);
                                } else {
                                    res.send(results);
                                }
                            });
                        }
                        else
                        {
                            res.send('Exista programari, nu putem sterge serviciul si nici categoria.');
                        }

                    }
                });
            }
        }
    });
});

module.exports = Router;

