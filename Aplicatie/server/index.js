const express = require('express');
const db = require('./dbconn');
const bodyParser = require('body-parser');
const CategoriiServiciiRoutes = require('./routes/categorii_servicii');
const ServiciiRoutes = require('./routes/servicii');
const UtilizatoriRoutes = require('./routes/utilizatori');
const FileUpload = require('./routes/file_upload');
const cors = require('cors');
const app = express();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({
    extended: true
}));

app.use('/categorii-servicii', CategoriiServiciiRoutes);

app.use('/servicii', ServiciiRoutes);

app.use('/utilizatori', UtilizatoriRoutes);

app.use('/upload', FileUpload);

app.listen(3010, () => {
    console.log("running on port 3010");
})