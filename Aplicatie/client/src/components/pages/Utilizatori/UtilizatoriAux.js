import React from "react";
import { useNavigate } from "react-router-dom"
import Axios from "axios";

function UtilizatoriAux(props) {
    
    let navigate = useNavigate();

    const deleteUtilizator = (id) => {
        Axios.delete(`http://localhost:3010/utilizatori/delete/${id}`).then((response) => {
            if(response.data==="Nu poate fi sters.")
            {
                alert("Nu poate fi sters, are programari");
            } else 
            {
                window.location.reload(false);
            }
        });
    }

    const redirect2 = (id) => {
        navigate(`/admin/utilizatori/edit/${id}`);
    }

    return (
                <div className="card">
                    <img className="card-img-top" alt={props.Nume} src={`/imagini/${props.Imagine}`} />
                    <div className="card-body">
                        <h2 className="card-title">{props.Rol_Website}</h2> 
                        <p className="card-text">{props.Nume} - {props.Prenume}</p>
                        <p className="card-text">{props.Nume_Utilizator}</p>
                        <p className="card-text">{props.Email}</p>
                        <p className="card-text">{props.Telefon}</p>
                        <div className="row">
                            <div className="col-sm">
                                <p className="puncte card-text">Puncte: {props.Puncte}</p>
                            </div>
                            <div className="col-sm">
                                <p className="aprecieri card-text">Aprecieri: {props.Aprecieri} ☆</p>
                            </div>
                        </div> 
                        <br /><br />
                        <div className="row justify-content-center" id="row-button">
                            <div className="d-inline float-left" id="utilizatori-edit"><button id="edit-button-utilizatori" onClick={() => {redirect2(props.ID)}}>Editeaza</button></div>
                            <div className="d-inline float-right" id="utilizatori-delete"><button id="delete-button-utilizatori" onClick={() => {deleteUtilizator(props.ID)}}>Sterge</button></div>
                            </div>    
                        </div> 
                    </div>
    )

}

export default UtilizatoriAux;