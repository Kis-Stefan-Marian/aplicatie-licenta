import React from "react";
import Axios from "axios";
import { useState } from 'react';
import { useNavigate } from "react-router-dom";

function InserareUtilizator() {

    let navigate = useNavigate();

    const [file, setFile] = useState();
    const [fileName, setFilename] = useState('Alege un fisier');
    const [rolwebsite, setRolwebsite] = useState('');
    const [nume, setNume] = useState('');
    const [prenume, setPrenume] = useState('');
    const [numeutilizator, setNumeutilizator] = useState('');
    const [email, setEmail] = useState('');
    const [telefon, setTelefon] = useState('');
    const [parola, setParola] = useState('');
    const [puncte, setPuncte] = useState('');
    const [aprecieri, setAprecieri] = useState('');

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    };

    const adaugaPoza = async(e) => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
       
        Axios.put(`http://localhost:3010/upload/${numeutilizator}`, formData).then((response) => {
          console.log(formData);
        })
    }

    const redirect = () => {
        navigate('/admin/utilizatori');
    }

    const inserareutilizator = () => {
        Axios.post('http://localhost:3010/utilizatori/inserare',{
            Rol_Website: rolwebsite,
            Nume: nume,
            Prenume: prenume,
            Nume_Utilizator: numeutilizator,
            Email: email,
            Telefon: telefon,
            Parola: parola,
            Puncte: puncte,
            Aprecieri: aprecieri,
            Imagine: fileName
        }
        ).then((response) => {
        adaugaPoza();
        redirect();
    });
    }
    
    return (
        <div className="container">
            <div className="py-4">
                <p><a id="back-edit" href="/admin/utilizatori/"><i className="fa fa-arrow-left" aria-hidden="true"></i> Înapoi</a></p><br />
                <h5>Insereaza un nou utilizator</h5><br />
                <label>Rol Website*</label><br />
                <input type="text" size="47.9" name="Rol_Website" placeholder="Tipul utilizatorului..." onChange={(e) => {setRolwebsite(e.target.value)}}></input><br /><br />
                <label>Nume*</label><br />
                <input type="text" size="47.9" name="Nume" placeholder="Nume..." onChange={(e) => {setNume(e.target.value)}}></input><br /><br />
                <label>Prenume*</label><br />
                <input type="text" size="47.9" name="Prenume" placeholder="Prenume..." onChange={(e) => {setPrenume(e.target.value)}}></input><br /><br />
                <label>Nume utilizator*</label><br />
                <input type="text" size="47.9" name="Nume_Utilizator" placeholder="Nume utilizator..." onChange={(e) => {setNumeutilizator(e.target.value)}}></input><br /><br />
                <label>Email*</label><br />
                <input type="text" size="47.9" name="Email" placeholder="Email..." onChange={(e) => {setEmail(e.target.value)}}></input><br /><br />
                <label>Telefon*</label><br />
                <input type="text" size="47.9" name="Telefon" placeholder="Telefon..." onChange={(e) => {setTelefon(e.target.value)}}></input><br /><br />
                <label>Parola*</label><br />
                <input type="text" size="47.9" name="Parola" placeholder="Parola..." onChange={(e) => {setParola(e.target.value)}}></input><br /><br />
                <label>Puncte*</label><br />
                <input type="text" size="47.9" name="Puncte" placeholder="Puncte..." onChange={(e) => {setPuncte(e.target.value)}}></input><br /><br />
                <label>Aprecieri*</label><br />
                <input type="text" size="47.9" name="Aprecieri" placeholder="Aprecieri..." onChange={(e) => {setAprecieri(e.target.value)}}></input><br /><br />
                <label>Imagine*</label><br />
                <input type="file" onChange={saveFile} />
                <div className="option-categories">
                    <button id="adauga-categorie-noua" onClick={() => { inserareutilizator() }}>Adaugă utilizatorul</button>
                </div>
            </div>
        </div>
    )
}

export default InserareUtilizator;