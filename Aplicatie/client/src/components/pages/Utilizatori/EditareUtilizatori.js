import React from 'react'
import { useParams } from 'react-router';
import './Utilizatori.css';
import { useState, useEffect } from "react";
import Axios from "axios";
import EditareUtilizatoriAux from './EditareUtilizatoriAux';

function EditUtilizator() {

    let {id} = useParams();
    const [utilizatori, setUtilizatori] = useState([]);
    const [nullList, setNullList] = useState(false);

    const editutilizator = () => {
        Axios.get(`http://localhost:3010/utilizatori/vizualizare/${id}`).then((response) => {
            setUtilizatori(response.data);
            if(response.data.length === 0)
            {
                setNullList(true);
            }
        });
    }

    useEffect(() => {
        editutilizator();
    }, [] );

    return (
        <div className="container">
            <div className="py-4">
                <p><a id="back-edit" href="/admin/utilizatori/"><i className="fa fa-arrow-left" aria-hidden="true"></i> Înapoi</a></p><br />
                <h3>Bun venit in panoul de editare!</h3><br />
                {utilizatori.map((val, key) => {
                return (
                    <EditareUtilizatoriAux ID = {val.ID} Nume = {val.Nume} Prenume = {val.Prenume} Rol_Website = {val.Rol_Website} Nume_Utilizator = {val.Nume_Utilizator} Email = {val.Email} Telefon = {val.Telefon} Parola = {val.Parola} Imagine = {val.Imagine} key={val.ID}/>
                )})}
            </div>
        </div>
    )
    }

export default EditUtilizator