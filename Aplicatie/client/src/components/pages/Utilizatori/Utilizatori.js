import React from 'react'
import './Utilizatori.css'
import Axios from 'axios'
import UtilizatoriAux from './UtilizatoriAux'
import { useState, useEffect } from 'react'
import { useNavigate } from "react-router-dom"

function Utilizatori() {

    let navigate = useNavigate();

    const [utilizatoriList, setUtilizatoriList] = useState([]);

    const getUtilizatori = () => {
        Axios.get("http://localhost:3010/utilizatori/vizualizare").then((response) => {
            setUtilizatoriList(response.data);
        })
    }

    const redirect3 = () => {
        navigate('/admin/utilizatori/inserare');
    }

    useEffect(() => {
        getUtilizatori();
    }, [] );

    return (
        <div className="container">
            <div className="py-4">
                <button id="adauga-categorie-noua" onClick={() => {redirect3()}}>Adaugă un utilizator nou</button>
                <br />
                <br />
                {utilizatoriList.map((val, key) => {
                    return (
                    <div className="utilizatori row justify-content-center">
                        <div className="col-sm-6 col-md-4">
                            <UtilizatoriAux ID = {val.ID} Imagine = {val.Imagine} Rol_Website = {val.Rol_Website} Nume = {val.Nume} Prenume = {val.Prenume} Nume_Utilizator = {val.Nume_Utilizator} Email = {val.Email} Telefon = {val.Telefon} Puncte = {val.Puncte} Aprecieri = {val.Aprecieri} key = {val.ID}/>
                        </div>
                    </div>
                )})}
            </div>
        </div>
    )

}

export default Utilizatori