import React from "react";
import Axios from "axios";
import { useState } from 'react';
import { useParams } from 'react-router';

function EditareUtilizatoriAux(props) {

    let {id} = useParams();

    const [file, setFile] = useState();
    const [fileName, setFilename] = useState('Alege un fisier');
    const [rolwebsite, setRolWebsite] = useState('');
    const [numeutilizator, setNumeutilizator] = useState('');
    const [email, setEmail] = useState('');
    const [telefon, setTelefon] = useState('');
    const [parola, setParola] = useState('');

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    };

    const adaugaPoza = async(e) => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
       
        Axios.put(`http://localhost:3010/upload/update/${id}`, formData).then((response) => {
          console.log(formData);
        })
    }

    const updateutilizator = (id) => {
        Axios.put(`http://localhost:3010/utilizatori/editare/${id}`,{
            Rol_Website: rolwebsite,
            Nume_Utilizator: numeutilizator,
            Email: email,
            Telefon: telefon,
            Parola: parola,
            Imagine: fileName
        }
        ).then((response) => {
        adaugaPoza(); 
        window.location.reload(false);  
    });
    }
    
    return (
        <div>
            <img className="card-img-top" alt={props.Nume} src={`/imagini/${props.Imagine}`} /><br />
            <label>Nume și Prenume: <b>{props.Nume} {props.Prenume}</b></label><br />
            <label>Rolul pe website: <b>{props.Rol_Website}</b></label><br /><br />
            <label>Email actual: <i>{props.Email}</i></label><br />
            <input type="text" size="47.9" name="Email" placeholder="Setati noua adresă de email..." onChange={(e) => {setEmail(e.target.value)}}></input><br /><br />
            <label>Numărul actual de telefon: <i>{props.Telefon}</i></label><br />
            <input type="text" size="47.9" name="Telefon" placeholder="Introduceți noul număr de telefon..." onChange={(e) => {setTelefon(e.target.value)}}></input><br /><br />
            <label>Parola actuală a utilizatorului: <i>{props.Parola}</i></label><br />
            <input type="text" size="47.9" name="Parola" placeholder="Setați noua parolă..." onChange={(e) => {setParola(e.target.value)}}></input><br />
            <label>Imagine*</label><br />
            <input type="file" onChange={saveFile} />

            <br /><br /><div className="option-categories">
            <button id="adauga-categorie-noua" onClick={() => {updateutilizator(id)}}>Modifică utilizatorul</button>
            </div>
        </div>
    )
}

export default EditareUtilizatoriAux;