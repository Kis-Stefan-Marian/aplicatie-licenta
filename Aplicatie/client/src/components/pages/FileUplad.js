import React, { useState } from 'react';
import axios from 'axios';

const FileUpload = () => {
  
const [file, setFile] = useState();
const [fileName, setFilename] = useState('Alege un fisier');

const saveFile = (e) => {
  setFile(e.target.files[0]);
  setFilename(e.target.files[0].name);
};
  
const adaugaPoza = async(e) => {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("fileName", fileName);
 
  axios.post("http://localhost:3010/upload", formData).then((response) => {
    console.log(formData);
  })
}

return (
<div>
  <input type="file" onChange={saveFile} />
  <button onClick={adaugaPoza}>Test</button>
</div>
  );
};

export default FileUpload;