import React from 'react'
import './Home.css'

const Home = () => {
    return (
        <div className="container">
            <div className="py-4">
                <h1 className="align-middle">Panoul de administrator!</h1>
            </div>
        </div>
    )
}

export default Home