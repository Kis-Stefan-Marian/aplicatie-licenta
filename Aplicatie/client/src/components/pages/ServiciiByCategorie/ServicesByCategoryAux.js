import React from "react";
import Axios from "axios";

function ServicesByCategoryAux(props) {

    const deleteService = (id) => {
        Axios.delete(`http://localhost:3010/servicii/delete/${id}`).then((response) => {
        alert(response.data);  
        window.location.reload(false);  
    });
    }

    return (
        <div>
            <div className="title-categories"><strong><h5>{props.Denumire}</h5></strong></div>
            <div className="description-categories">{props.Descriere}</div>
            <div className="option-categories">
                <div className="d-inline"><a id="edit-button" href="/admin/servicii">Editeaza</a></div>
                <div className="d-inline"> | </div>
                <div className="d-inline"><button id="delete-button" onClick={() => deleteService(props.Cod_Serviciu)}>Sterge</button></div>
            </div>
        </div>
    )
}

export default ServicesByCategoryAux;