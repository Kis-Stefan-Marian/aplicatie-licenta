import React from 'react'
import { useParams } from 'react-router';
import '../Servicii/Services.css';
import { useState, useEffect } from "react";
import Axios from "axios";
import './ServicesByCategory.css';
import ServicesByCategoryAux from './ServicesByCategoryAux';

function Services() {

    let {id} = useParams();
    const [servicesbycategoryList, setServicesByCategoryList] = useState([]);
    const [nullList, setNullList] = useState(false);
    
    const getServicesByCategory = () => {
        Axios.get(`http://localhost:3010/servicii/vizualizarebycat/${id}`).then((response) => {
            setServicesByCategoryList(response.data);
            if(response.data.length === 0)
            {
                setNullList(true);
            }
        });
    }

    useEffect(() => {
        getServicesByCategory();
    }, [] );

    return (
        <div className="container">
            <div className="py-4">
                <div><a className="back-button" href="/admin/categorii"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a></div>
                { nullList && ( <div><h4>Nu exista servicii in aceasta categorie.</h4></div> )}
                {servicesbycategoryList.map((val, key) => {
                return (
                    <ServicesByCategoryAux Denumire = {val.Denumire} Descriere = {val.Descriere} Cod_Serviciu = {val.Cod_Serviciu} />
                )})}
            </div>
        </div>
    )
}

export default Services