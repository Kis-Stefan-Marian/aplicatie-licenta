import React from "react";
import Axios from "axios";
import { useNavigate } from "react-router-dom"

function ServicesAux(props) {

    let navigate = useNavigate();

    const deleteService = (id) => {
        Axios.delete(`http://localhost:3010/servicii/delete/${id}`).then((response) => {
        window.location.reload(false);  
    });
    }

    const redirect2 = (id) => {
        navigate(`/admin/servicii/edit/${id}`);
    }

    return (
        <div>
            <div className="title-categories"><strong><h5 className="d-inline">{props.Denumire}</h5></strong> <p className="d-inline"> - Cod Categorie: {props.Cod_Categorie}</p></div>
            <div className="d-inline"><p className="d-inline">Pret: {props.Pret} lei</p> | <p className="d-inline">Durata: {props.Durata} minute</p> | <p className="d-inline">Puncte: {props.Puncte} puncte</p></div>
            <br /><br /><br />
            <div className="description-categories">{props.Descriere}</div>
            <div className="option-categories">
            <div className="d-inline"><button id="edit-button" onClick={() => {redirect2(props.Cod_Serviciu)}}>Editeaza</button></div>
                <div className="d-inline"> | </div>
                <div className="d-inline"><button id="delete-button" onClick={() => deleteService(props.Cod_Serviciu)}>Sterge</button></div>
            </div>
        </div>
    )
}

export default ServicesAux;