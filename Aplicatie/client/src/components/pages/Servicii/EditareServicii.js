import React from 'react'
import { useParams } from 'react-router';
import './Services.css';
import { useState, useEffect } from "react";
import Axios from "axios";
import EditareServiciiAux from './EditareServiciiAux';

function EditServicii() {

    let {id} = useParams();
    const [servicii, setServicii] = useState([]);
    const [nullList, setNullList] = useState(false);

    const editServicebyId = () => {
        Axios.get(`http://localhost:3010/servicii/vizualizare/${id}`).then((response) => {
            setServicii(response.data);
            if(response.data.length === 0)
            {
                setNullList(true);
            }
        });
    }

    useEffect(() => {
        editServicebyId();
    }, [] );

    return (
        <div className="container">
            <div className="py-4">
                <p><a id="back-edit" href="/admin/servicii/"><i class="fa fa-arrow-left" aria-hidden="true"></i> Înapoi</a></p><br />
                <h3>Bun venit in panoul de editare!</h3><br />
                {servicii.map((val, key) => {
                return (
                    <EditareServiciiAux Cod_Categorie = {val.Cod_Categorie} Denumire = {val.Denumire} Descriere = {val.Descriere} Pret = {val.Pret} Durata = {val.Durata} Puncte = {val.Puncte}  key={val.Cod_Serviciu}/>
                )})}
            </div>
        </div>
    )
    }

export default EditServicii