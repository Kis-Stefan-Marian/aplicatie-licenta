import React from "react";
import Axios from "axios";
import { useState } from 'react';
import { useNavigate } from "react-router-dom"

function InserareServiciu() {

    let navigate = useNavigate();

    const [categorieserviciu, setCategorieserviciu] = useState('');
    const [denumireserviciu, setDenumireserviciu] = useState('');
    const [descriereserviciu, setDescriereserviciu] = useState('');
    const [pretserviciu, setPretserviciu] = useState('');
    const [durataserviciu, setDurataserviciu] = useState('');
    const [puncteserviciu, setPuncteserviciu] = useState('');

    const redirect = () => {
        navigate('/admin/servicii');
    }

    const inserareserviciu = () => {
        Axios.post('http://localhost:3010/servicii/inserare',{
            Cod_Categorie: categorieserviciu,
            Denumire: denumireserviciu,
            Descriere: descriereserviciu,
            Pret: pretserviciu,
            Durata: durataserviciu,
            Puncte: puncteserviciu
        }
        ).then((response) => {
        if(response.data==="Serviciul exista.")
        {
            alert("Serviciul exista pe website. Inserati un serviciu nou.");
            window.location.reload(false);
        } else {
        redirect();
        } 
    });
    }
    
    return (
        <div className="container">
            <div className="py-4">
                <p><a id="back-edit" href="/admin/servicii/"><i class="fa fa-arrow-left" aria-hidden="true"></i> Înapoi</a></p><br />
                <h5>Inserează un serviciu nou.</h5>
                <br />
                <label>Codul Categoriei*</label><br />
                <input type="text" size="47.9" name="Cod_Categorie" placeholder="Codul categoriei..." onChange={(e) => {setCategorieserviciu(e.target.value)}}></input><br /><br />
                <label>Denumire serviciu*</label><br />
                <input type="text" size="47.9" name="Denumire_Serviciu" placeholder="Denumirea serviciului..." onChange={(e) => {setDenumireserviciu(e.target.value)}}></input><br /><br />
                <label>Descriere serviciu*</label><br />
                <textarea cols="50" rows="5" name="Descriere_Serviciu" placeholder="Descrierea serviciului..." onChange={(e) => {setDescriereserviciu(e.target.value)}}></textarea><br /><br />
                <label>Pret serviu*</label><br />
                <input type="text" size="47.9" name="Pret_Serviciu" placeholder="Pretul serviciului..." onChange={(e) => {setPretserviciu(e.target.value)}}></input><br /><br />
                <label>Durata serviciu*</label><br />
                <input type="text" size="47.9" name="Durata_Serviciu" placeholder="Durata serviciului..." onChange={(e) => {setDurataserviciu(e.target.value)}}></input><br /><br />
                <label>Puncte serviciu*</label><br />
                <input type="text" size="47.9" name="Puncte_Serviciu" placeholder="Puncte serviciu..." onChange={(e) => {setPuncteserviciu(e.target.value)}}></input><br /><br />
                <div className="option-categories">
                    <button id="adauga-categorie-noua" onClick={() => { inserareserviciu() }}>Inserează serviciu</button>
                </div>
            </div>
        </div>
    )
}

export default InserareServiciu;