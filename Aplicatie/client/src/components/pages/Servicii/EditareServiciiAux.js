import React from "react";
import Axios from "axios";
import { useState } from 'react';
import { useParams } from 'react-router';

function EditareServiciiAux(props) {

    let {id} = useParams();
    const [descriereNoua, setDescriere] = useState('');
    const [pretNou, setPret] = useState('');
    const [durataNoua, setDurata] = useState('');
    const [puncte, setPuncte] = useState('');

    const updateserviciu = (id) => {
        Axios.put(`http://localhost:3010/servicii/editare/${id}`,{
            Descriere: descriereNoua,
            Pret: pretNou,
            Durata: durataNoua,
            Puncte: puncte
        }
        ).then((response) => {
        window.location.reload(false);  
    });
    }
    
    return (
        <div>
            <label>Denumirea serviciului pe care doriti sa îl editati: <b>{props.Denumire}</b></label><br />
            <label>Categoria părinte: <b>{props.Cod_Categorie}</b></label><br /><br />
            <label><b>Descrierea acuala a serviciului: </b><i>{props.Descriere}</i></label><br /><br />
            <label>Descrierea noua a categoriei: </label><br />
            <textarea cols="50" rows="5" name="Descriere_Categorie" placeholder="Tastati aici..." onChange={(e) => {setDescriere(e.target.value)}}></textarea><br /><br />
            <label>Pretul actual al serviciului: <i>{props.Pret} Lei</i></label><br />
            <input type="text" size="47.9" name="Pret_Serviciu" placeholder="Setati pretul nou al serviciului..." onChange={(e) => {setPret(e.target.value)}}></input><br /><br />
            <label>Durata actuală a serviciului: <i>{props.Durata} minute</i></label><br />
            <input type="text" size="47.9" name="Durata_Serviciu" placeholder="Setati noua durată a serviciului..." onChange={(e) => {setDurata(e.target.value)}}></input><br /><br />
            <label>Valoarea actuală în puncte a serviciului: <i>{props.Puncte} puncte</i></label><br />
            <input type="text" size="47.9" name="Puncte_Serviciu" placeholder="Setați valoarea nouă în puncte a serviciului..." onChange={(e) => {setPuncte(e.target.value)}}></input><br />

            <br /><br /><div className="option-categories">
            <button id="adauga-categorie-noua" onClick={() => {updateserviciu(id)}}>Modifică serviciul actual</button>
            </div>
        </div>
    )
}

export default EditareServiciiAux;