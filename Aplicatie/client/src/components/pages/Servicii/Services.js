import React from 'react'
import './Services.css';
import Axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom"
import ServicesAux from "./ServicesAux";
function Services() {
    
    let navigate = useNavigate();

    const [servicesList, setServicesList] = useState([]);

    const getServices = () => {
        Axios.get("http://localhost:3010/servicii/vizualizare").then((response) => {
            setServicesList(response.data);
        })
    }

    const redirect3 = () => {
        navigate('/admin/servicii/inserare');
    }

    useEffect(() => {
        getServices();
    }, [] );
    
    return (
        <div className="container">
            <div className="py-4">
            <button id="adauga-categorie-noua" onClick={() => {redirect3()}}>Adaugă un serviciu nou</button>
            {servicesList.map((val, key) => {
                return <ServicesAux Cod_Categorie = {val.Cod_Categorie} Denumire = {val.Denumire} Descriere = {val.Descriere} Pret = {val.Pret} Durata = {val.Durata} Puncte = {val.Puncte} Cod_Serviciu = {val.Cod_Serviciu} />
            })}
            </div>
        </div>
    )
}

export default Services