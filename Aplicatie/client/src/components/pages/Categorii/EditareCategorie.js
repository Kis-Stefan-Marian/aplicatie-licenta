import React from 'react'
import { useParams } from 'react-router';
import './Categories.css';
import { useState, useEffect } from "react";
import Axios from "axios";
import EditareCategorieAux from './EditareCategorieAux';

function EditCategorii() {

    let {id} = useParams();
    const [categorii, setCategorii] = useState([]);
    const [nullList, setNullList] = useState(false);

    const editCategoriiById = () => {
        Axios.get(`http://localhost:3010/categorii-servicii/vizualizare/${id}`).then((response) => {
            setCategorii(response.data);
            if(response.data.length === 0)
            {
                setNullList(true);
            }
        });
    }
    useEffect(() => {
        editCategoriiById();
    }, [] );

    return (
        <div className="container">
            <div className="py-4">
                <p><a id="back-edit" href="/admin/categorii/"><i class="fa fa-arrow-left" aria-hidden="true"></i> Înapoi</a></p><br />
                <h3>Bun venit in panoul de editare!</h3><br />
            {categorii.map((val, key) => {
                return (
                    <EditareCategorieAux Denumire = {val.Denumire} Descriere = {val.Descriere} Cod_Serviciu = {val.Cod_Serviciu}  key={val.Cod_Serviciu}/>
                )})}
            </div>
        </div>
    )
    }

export default EditCategorii