import React from 'react'
import './Categories.css';
import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom'
import Axios from "axios";
import CategoriesAux from './CategoriesAux';

function Categories() {

    let navigate = useNavigate();
    const [categoriesList, setCategoriesList] = useState([]);

    const getCategories = () => {
        Axios.get("http://localhost:3010/categorii-servicii/vizualizare/").then((response) => {
            setCategoriesList(response.data);
        });
    }

    const redirect3 = () => {
        navigate('/admin/categorii/inserare');
    }

    useEffect(() => {
        getCategories();
    }, [] );

    return (
        <div className="container">
            <div className="py-4">
            <button id="adauga-categorie-noua" onClick={() => {redirect3()}}>Adaugă o categorie nouă</button>
            {categoriesList.map((val, key) => {
                return <CategoriesAux Cod_Categorie = {val.Cod_Categorie} Denumire = {val.Denumire} Descriere = {val.Descriere}/>
            })}
            </div>
        </div>
    )
}

export default Categories