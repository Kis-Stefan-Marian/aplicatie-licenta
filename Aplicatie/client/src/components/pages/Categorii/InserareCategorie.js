import React from "react";
import Axios from "axios";
import { useState } from 'react';
import { useNavigate } from "react-router-dom";

function InserareCategorie() {

    let navigate = useNavigate();

    const [denumirecategorie, setDenumirecategorie] = useState('');
    const [descrierecategorie, setDescrierecategorie] = useState('');

    const redirect = () => {
        navigate('/admin/categorii');
    }

    const inserarecategorie = () => {
        Axios.post('http://localhost:3010/categorii-servicii/inserare',{
            Denumire: denumirecategorie,
            Descriere: descrierecategorie
        }
        ).then((response) => {
        if(response.data==="Categoria exista deja.")
        {
            alert("Categoria exista deja. Introduceti o alta denumire.");
            window.location.reload(false);
        } else {
            redirect();
        } 
    });
    }
    
    return (
        <div className="container">
            <div className="py-4">
                <p><a id="back-edit" href="/admin/categorii/"><i class="fa fa-arrow-left" aria-hidden="true"></i> Înapoi</a></p><br />
                <h5>Inserează o categorie nouă.</h5>
                <br />
                <label>Denumire categorie*</label><br />
                <input type="text" size="47.9" name="Denumire_Categorie" placeholder="Denumirea categoriei..." onChange={(e) => {setDenumirecategorie(e.target.value)}}></input><br /><br />
                <label>Descriere categorie*</label><br />
                <textarea cols="50" rows="5" name="Descriere_Categorie" placeholder="Tastati aici..." onChange={(e) => {setDescrierecategorie(e.target.value)}}></textarea>
                <br /><br />
                <div className="option-categories">
                <button id="adauga-categorie-noua" onClick={() => { inserarecategorie() }}>Inserează categoria</button>
                </div>
            </div>
        </div>
    )
}

export default InserareCategorie;