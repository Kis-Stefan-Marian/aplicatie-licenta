import React from "react";
import Axios from "axios";
import { useState } from 'react';
import { useParams } from 'react-router';

function EditareCategorieAux(props) {

    let {id} = useParams();
    const [descriereNoua, setDescriere] = useState('');

    const updatecategorie = (id) => {
        Axios.put(`http://localhost:3010/categorii-servicii/editare/${id}`,{
            Descriere: descriereNoua,
        }
        ).then((response) => {
        window.location.reload(false);  
    });
    }
    
    return (
        <div>
            <label>Denumirea categoriei pe care doriti sa o editati: <b>{props.Denumire}</b></label><br />
            <label>Descrierea acuala a categoriei: <i>{props.Descriere}</i></label><br /><br />
            <label>Descrierea noua a categoriei: </label><br />
            <textarea cols="50" rows="5" name="Descriere_Categorie" placeholder="Tastati aici..." onChange={(e) => {setDescriere(e.target.value)}}></textarea>

            <br /><br /><div className="option-categories">
            <button id="adauga-categorie-noua" onClick={() => {updatecategorie(id)}}>Modifică descrierea categoriei</button>
            </div>
        </div>
    )
}

export default EditareCategorieAux;