import React from 'react'
import { useNavigate } from 'react-router-dom'
import Axios from "axios";


function CategoriesAux(props) {

    let navigate = useNavigate();

    const redirect = (id) => {
        navigate(`/admin/services-by-category/${id}`);
    }

    const redirect2 = (id) => {
        navigate(`/admin/categorii/edit/${id}`);
    }

    const deleteCategory = (id) => {
        Axios.delete(`http://localhost:3010/categorii-servicii/delete/${id}`).then((response) => {
        alert(response.data);
        window.location.reload(false); 
    });
    }

    return (
        <div>
            <div className="title-categories"><strong><h5>{props.Denumire}</h5></strong></div><br />
            <div className="description-categories">{props.Descriere}</div>
            <div className="option-categories">
                <div className="d-inline"><button id="edit-button" onClick={() => {redirect2(props.Cod_Categorie)}}>Editeaza</button></div>
                <div className="d-inline"> | </div>
                <div className="d-inline"><button id="view-button" onClick={() => {redirect(props.Cod_Categorie)}}>Vizualizare</button></div>
                <div className="d-inline"> | </div>
                <div className="d-inline"><button id="delete-button" onClick={() => deleteCategory(props.Cod_Categorie)}>Sterge</button></div>
            </div>
        </div>
    )
}

export default CategoriesAux;