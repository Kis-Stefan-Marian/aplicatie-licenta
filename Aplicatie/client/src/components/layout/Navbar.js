import React from 'react'
import './Navbar.css';

const Navbar = () => {

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-primary justify-content-center flex-column">
            <a className="navbar-brand text-light" href="/">Management Service Auto</a>
            <ul className="nav flex-column">
                <li className="nav-item has-submenu">
                    <a className="nav-link text-light" href='/admin/panou'>Panou</a>
                </li>
                <li className="nav-item has-submenu">
                    <a className="nav-link text-light" href='/admin/categorii'>Categorii</a>
                </li>
                <div className="submenu">
                    <li className="submenu collapse">
                        <a className="nav-link text-light" href='/admin/categorii'>Categorii</a>
                    </li>
                </div>
                <li className="nav-item has-submenu">
                    <a className="nav-link text-light" href='/admin/servicii'>Servicii</a>
                </li>
                <li className="nav-item has-submenu">
                    <a className="nav-link text-light" href='/admin/utilizatori'>Utilizatori</a>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar