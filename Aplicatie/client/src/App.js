import React from 'react'
import './App.css'
import Home from './components/pages/Home'
import Categories from './components/pages/Categorii/Categories'
import Services from './components/pages/Servicii/Services'
import Navbar from './components/layout/Navbar'
import ServicesbyCategory from './components/pages/ServiciiByCategorie/ServicesByCategory'
import Utilizatori from './components/pages/Utilizatori/Utilizatori'
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import FileUpload from './components/pages/FileUplad'
import EditCategorii from './components/pages/Categorii/EditareCategorie'
import EditServicii from './components/pages/Servicii/EditareServicii'
import EditUtilizator from './components/pages/Utilizatori/EditareUtilizatori'
import InserareCategorie from './components/pages/Categorii/InserareCategorie'
import InserareServiciu from './components/pages/Servicii/InserareServiciu'
import InserareUtilizator from './components/pages/Utilizatori/InserareUtilizator'

function App() {
    return (
        <Router>
            <div className="App">
                <div className="container-l">
                    <div className="row row-dashboard">
                        <div className="col-2 bg-primary">
                            <Navbar />
                        </div>
                        <div className="col-10">
                        <Routes>
                            <Route path="/admin/panou" element={<Home/>} />
                            <Route path="/admin/categorii" element={<Categories/>} />
                            <Route path="/admin/servicii" element={<Services/>} />
                            <Route path="/admin/services-by-category/:id" element={<ServicesbyCategory/>} />
                            <Route path="/admin/utilizatori" element={<Utilizatori/>} />
                            <Route path="/admin/file-upload" element={<FileUpload/>} />
                            <Route path="/admin/categorii/edit/:id" element={<EditCategorii/>} />
                            <Route path="/admin/servicii/edit/:id" element={<EditServicii/>} />
                            <Route path="/admin/utilizatori/edit/:id" element={<EditUtilizator/>} />
                            <Route path="/admin/categorii/inserare" element={<InserareCategorie />} />
                            <Route path="/admin/servicii/inserare" element={<InserareServiciu />} />
                            <Route path="/admin/utilizatori/inserare" element={<InserareUtilizator />} />
                        </Routes>
                        </div>
                    </div>
                </div>
            </div>
        </Router>
    )
}

export default App